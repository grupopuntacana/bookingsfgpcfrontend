const express = require('express');
const history = require('connect-history-api-fallback');
const serveStatic = require('serve-static');
const path = require('path');
const morgan = require('morgan');

const app = express();
app.use(history({ index: '/' }));

app.enable('trust proxy');

app.use(morgan('combined'));
app.use(serveStatic(path.join(__dirname, 'dist')));

const port = process.env.PORT || 80;
app.listen(port);