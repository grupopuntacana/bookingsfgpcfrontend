const { defineConfig } = require('@vue/cli-service')
const CompressionPlugin = require("compression-webpack-plugin")
module.exports = defineConfig({
  transpileDependencies: true
})

module.exports = {
  devServer: {
    proxy: {
      '^/api': {
        // target: 'http://127.0.0.1:8000',
        target: 'https://bookingsfgpc-develop.puntacana.com',
        changeOrigin: true
      },
    }
  },
  configureWebpack: {
    plugins: [
      new CompressionPlugin(),
    ]
  }
}
