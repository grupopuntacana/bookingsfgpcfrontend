FROM node:latest as build-stage

WORKDIR /code

COPY package.json ./

RUN npm install

COPY . .

ARG VUE_APP_APPLICATION_ID

ENV VUE_APP_APPLICATION_ID $VUE_APP_APPLICATION_ID

RUN npm run build

EXPOSE 80

CMD npm run start
